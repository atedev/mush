<?php

namespace Mush\RoomLog\Entity;

interface LogParameter
{
    public function getClassName(): string;
}
