<?php

namespace Mush\Status\Enum;

class InjuryEnum
{
    public const BURNS_50_OF_BODY = 'burns_50_of_body';
    public const BURNS_90_OF_BODY = 'burns_90_of_body';
    public const BROKEN_FINGER = 'broken_finger';
    public const BROKEN_FOOT = 'broken_foot';
    public const BROKEN_LEG = 'broken_leg';
    public const BROKEN_RIBS = 'broken_ribs';
    public const BROKEN_SHOULDER = 'broken_shoulder';
    public const BRUISED_SHOULDER = 'bruised_shoulder';
    public const BURNT_ARMS = 'burnt_arms';
    public const BURNT_HAND = 'burnt_hand';
    public const BURST_NOSE = 'burst_nose';
    public const BUSTED_ARM_JOINT = 'busted_arm_joint';
    public const BUSTED_SHOULDER = 'busted_shoulder';
    public const HAEMORRHAGE_CRITICAL = 'haemorrhage_critical';
    public const DAMAGED_EARS = 'damaged_ears';
    public const DESTROYED_EARS = 'destroyed_ears';
    public const DISFUNCTIONAL_LIVER = 'disfunctional_liver';
    public const HAEMORRHAGE = 'haemorrhage';
    public const HAEMORRHAGE_MINOR = 'haemorrhage_minor';
    public const HEAD_TRAUMA = 'head_trauma';
    public const IMPLANTED_BULLET = 'implanted_bullet';
    public const INNER_EAR_DAMAGED = 'inner_ear_damaged';
    public const MASHED_FOOT = 'mashed_foot';
    public const MASHED_HAND = 'mashed_hand';
    public const MISSING_FINGER = 'missing_finger';
    public const OPEN_AIR_BRAIN = 'open_air_brain';
    public const PUNCTURED_LUNG = 'punctured_lung';
    public const SMASHED_ARMS = 'smashed_arms';
    public const SMASHED_LEGS = 'smashed_legs';
    public const TORN_TONGUE = 'torn_tongue';
}
