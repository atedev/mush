<?php

namespace Mush\User\Enum;

class RoleEnum
{
    public const USER = 'user';
    public const ADMIN = 'admin';
}
