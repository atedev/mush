export const NO_ALERT = 'no.alert';
const LOW_OXYGEN = 'low.oxygen';
const LOW_HULL = 'low.hull';
const NUMBER_FIRE = 'number.fire';
const BROKEN_DOORS = 'broken.doors';
const BROKEN_EQUIPMENTS = 'broken.equipments';

export const AlertsIcons = {
    [NO_ALERT]: require('@/assets/images/alerts/infoalert.png'),
    [LOW_OXYGEN]: require('@/assets/images/alerts/o2alert.png'),
    [LOW_HULL]: require('@/assets/images/shield.png'),
    [NUMBER_FIRE]: require('@/assets/images/alerts/fire.png'),
    [BROKEN_DOORS]: require('@/assets/images/alerts/door.png'),
    [BROKEN_EQUIPMENTS]: require('@/assets/images/alerts/broken.png')
}
;
